msgid ""
msgstr ""
"Project-Id-Version: 1.0\n"
"POT-Creation-Date: 2011-11-29 08:55+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI +ZONE\n"
"Last-Translator: Frederic Peters <fpeters@entrouvert.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"Language-Code: fr\n"
"Language-Name: French\n"
"Preferred-Encodings: utf-8 latin1\n"
"Domain: plone.formwidget.captcha\n"

#: ./validator.py:15
msgid "The code you entered was wrong, please enter the new one."
msgstr "Le code que vous avez entré est erroné, veuillez entrer le nouveau."

